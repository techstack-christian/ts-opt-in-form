<?php

//Autoload Files
spl_autoload_register( function( $class) {
    $prefix = 'TechstackOF';
    $len = strlen($prefix);
    
    if(strncmp($prefix,$class, $len) !== 0 ) :
        return;
    endif;

    $path_arr = explode('\\', $class);
    $class_name = str_replace('_', '-', end($path_arr));
    array_pop($path_arr);
    $path = '';
    
    foreach($path_arr as $rclass):
        $path .= '/' . str_replace('_', '-', $rclass);
    endforeach;
    
    $allowed_file_prefix = [
        'class-',
        'traits-',
        'abstract-',
    ];
    
    foreach( $allowed_file_prefix as $index => $prefix ) :
        $check_location = TECHSTACK_OF_PLUGIN_DIR . 'inc/classes' . str_replace('/techstackof','', strtolower($path))  . "/$prefix" . strtolower($class_name) . '.php';
        
        if(file_exists( $check_location )):
            require_once $check_location;
            return;
        endif;
    endforeach;
});