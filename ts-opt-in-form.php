<?php
/**
 * Plugin Name: Techstack Opt-in Form
 * Plugin URI: #
 * Description: Techstack Opt-in Form Plugin
 * Version: 1.0.0
 * Author: Techstack
 * Author URI: https://techstack.com
 * Text Domain: ts-opt-in-form
**/

define( 'TECHSTACK_OF_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
define( 'TECHSTACK_OF_PLUGIN_URL', plugin_dir_url( __FILE__ ));
define( 'TECHSTACK_OF_PLUGIN_BASEFILE', plugin_basename(__FILE__));

/* Load Dependencies Under INC folder*/

foreach ( glob( dirname( __FILE__ ) . '/inc/*.php' ) as $file ) :
    include $file;
endforeach;